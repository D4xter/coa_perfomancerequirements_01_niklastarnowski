﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class EntityManager : MonoBehaviour, ILog
    {
        public void Log(object message)
            {
                Debug.Log(message);
            }
        

        public string[] entities;

        private void Start()
        {
            Debug.Log("Names are being registered");
            for (int i = 0; i < entities.Length; i++)
                {
                entities[i] = entities[i];
                }
                Debug.Log(entities);
            }
        }

    }
