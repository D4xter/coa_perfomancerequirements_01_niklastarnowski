﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Player : Entity, IAttack
    {
        [SerializeField]
        private int currentHealth;
        public int CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }

        [SerializeField]
        private int attackAmount;
        public int AttackAmount
        {
            get { return attackAmount; }
            set { attackAmount = value; }
        }

        [SerializeField]
        private bool isAlive;
        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        public Enemy enemyEntity;

        private void Start()
        {
            Debug.Log("Your name is Steve");
            Debug.Log("Your health is at " + currentHealth);
            Debug.Log("Your damage is at" + attackAmount);
        }

        public void AttackEnemy()
        {
            if (isAlive)
            {
                public void Damage(int AttackAmount)
                {
                    if (currentHealth < 0)
                    {
                        currentHealth = 0;
                        isAlive = false;
                        Debug.Log("You killed the Enemy with 0 hp");
                    }

                    else if (currentHealth <= 0)
                    {
                        currentHealth = 0;
                        isAlive = false;
                        Debug.Log("You killed the Enemy with 0 hp");
                    }

                    else
                    {
                        currentHealth -= AttackAmount;
                    }

                    Debug.Log("You damaged the enemy by" + attackAmount + "hp");
                }
            }
        }
    }
}
        
    



