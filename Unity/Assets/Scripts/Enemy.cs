﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Enemy : Entity, IDamagable
    {
        [SerializeField]
        private int currentHealth;
        public int CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }

        [SerializeField]
        private int attackAmount;
        public int AttackAmount
        {
            get { return attackAmount; }
            set { attackAmount = value; }
        }

        [SerializeField]
        private bool isAlive;
        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        public void Damage(int amount)
        {
        if (currentHealth < 0)
        { 
            currentHealth = 0;
            isAlive = false;
            Debug.Log("You killed the Enemy with 0 hp");
        }

        else if (currentHealth <= 0)
        {
            currentHealth = 0;
            isAlive = false;
            Debug.Log("You killed the Enemy with 0 hp");
        }

        else
        {
            currentHealth -= amount;
        }

        Debug.Log("You damaged the enemy by" + attackAmount + "hp");
        }
        
    }
}