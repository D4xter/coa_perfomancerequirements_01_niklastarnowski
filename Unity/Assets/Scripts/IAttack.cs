﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public interface IAttack
    {
        void AttackEnemy(string enemyName, int amount);
    }
}